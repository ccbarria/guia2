# guia2
1) calculo de promedio de 4 notas utilizando funcion

2) calculo de area de rectangulo utilizando funcion

3) calculo de area de un circulo utilizando funcion y constante PI

4) calculo de produccion en galones conociendo numero de litros, utilizando funcion

5) calculo de ganancia de funcionario que se cancela por hora y se ingresa horas trabajadas semanalmente, utilizando funcion

6) calculo de pulgadas a partir de cantidad de metros, utilizando funcion

7) calculo de area de un triangulo utilizando funcion

8) calculo a dolares a partir de moneda nacional, ocupando funcion

9) calculo de edad solicitando a�o de nacimiento, usando funcion

10) calculo valor estacionamiento por tiempo ocupado, usando funcion

11) calculo a pagar por pintar determinados metros cuadrados, usando funcion

12) calculo de valor hipotenusa, usando funcion

13) calculo valor a cancelar conociendo kilometros recorridos y valor por kilometros, usando funcion

14) calculo de tiempo conociendo distancia y velocidad, usando funcion

15) calculo de valor llamada teniendo minutos y valor por minuto, usando funcion

16) calculo valor consumo de agua, conociendo m3 utilizados y valor por metro cubico, usando funcion

17) calculo valor consumo de energia, conociendo consumo y valor kw por hora, usando funcion

18.- Realice un diagrama de flujo y pseudoc�digo que representen el algoritmo para determinar cu�nto pagar� 
     finalmente una persona por un art�culo equis, considerando que tiene un descuento de 20%, y debe pagar 
     15% de IVA (debe mostrar el precio con descuento y el precio final). 

19.- Realice un algoritmo para determinar cu�nto se debe pagar por equis cantidad de l�pices considerando que 
     si son 1000 o m�s el costo es de $85; de lo contrario, el precio es de $90. Repres�ntelo con el pseudoc�digo, 
     el diagrama de flujo. 

20.- Almacenes �El harapiento distinguido� tiene una promoci�n: a todos los trajes que tienen un precio superior a
     $25000 se les aplicar� un descuento de 15 %, a todos los dem�s se les aplicar� s�lo 8 %. Realice un algoritmo
     para determinar el precio final que debe pagar una persona por comprar un traje y de cu�nto es el descuento que
     obtendr�. Repres�ntelo mediante el pseudoc�digo, el diagrama de flujo. 

21.- Se requiere determinar cu�l de tres cantidades proporcionadas es la mayor. Realizar su respectivo algoritmo y 
     representarlo mediante un diagrama de flujo, pseudoc�digo. 

22.- �La langosta ahumada� es una empresa dedicada a ofrecer banquetes; sus tarifas son las siguientes: el costo de
     platillo por persona es de $9500, pero si el n�mero de personas es mayor a 200 pero menor o igual a 300, el costo 
     es de $8500. Para m�s de 300 personas el costo por platillo es de $7500. Se requiere un algoritmo que ayude a 
     determinar el presupuesto que se debe presentar a los clientes que deseen realizar un evento. Mediante pseudoc�digo, diagrama de flujo. 

23.- La asociaci�n de vinicultores tiene como pol�tica fijar un precio inicial al kilo de uva, la cual se clasifica en tipos A y B, y adem�s
     en tama�os 1 y 2. Cuando se realiza la venta del producto, �sta es de un solo tipo y tama�o, se requiere determinar cu�nto recibir� un 
     productor por la uva que entrega en un embarque, considerando lo siguiente: si es de tipo A, se le cargan $20 al precio inicial cuando 
     es de tama�o 1; y $30 si es de tama�o 2. Si es de tipo B, se rebajan $30 cuando es de tama�o 1, y $50 cuando es de tama�o 2. Realice un
     algoritmo para determinar la ganancia obtenida y repres�ntelo mediante diagrama de flujo y pseudoc�digo. 

